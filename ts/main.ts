/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const conf = require('./conf/conf.json');
import HttpServer from './src/HttpServer';
import logger from './src/logger';
import Agent from './src/ws/AgentManager';

import ClientsManager from './src/ws/ClientsManager';
import Message from './src/ws/Message';
logger.info('配置内容： ', conf);
let httpServer = new HttpServer();
httpServer.start(conf.httpPort);


let clientsManager = new ClientsManager();
clientsManager.start(conf.webSocketPort);


let agentManager = new Agent.AgentManager();
agentManager.start();

clientsManager.setToAgentMsgCallback((data: Message.MsgBase): boolean => {
    return agentManager.fromClientsMessage(data);
});

agentManager.setToClientMsgCallback((data:Message.MsgBase):boolean=>{
    return clientsManager.fromAgentMsg(data);
})