
import { RedisHost } from './../utils/mysqlHandler';
namespace Message {
    export type onMessageCb = (data: Message.MsgBase) => boolean;
    export enum Method {
        LOGIN_REQ = "loginReq",
        LOGIN_RESP = "loginResp",

        LOGOUT_REQ = "logoutReq",
        LOGOUT_RESP = "logoutResp",

        AICAHT_REQ = "aichatReq",
        AICAHT_RESP = "aichatResp",

        AGENT_INFO_REQ = 'agentInfoReq',
        AGENT_INFO_RESP = 'agentInfoResp',
    }

    export class MsgBase {
        method: string = '';
        uuid: string = '';
    }

    export class ClientMsgBase extends MsgBase{
        serviceId: number = -1;
        
    }

    export class LoginReq extends ClientMsgBase {
        method: string = 'loginReq';
        appid: string = '';
        uname: string = '';
    }
    export class LogoutReq extends ClientMsgBase {
        method: string = "logoutReq";
        appid: string = ""
    }
    export class AiChatReq extends ClientMsgBase {
        method: string = "aichatReq";
        appid: string = "";
        content: string = "";
    }


    export class AgentInfoReq extends MsgBase {
        redisCluster: RedisHost[] = [];
        mysqlAddr: string = "127.0.0.1";
        mysqlPort: number = 3306;
        mysqlUser: string = "root";
        mysqlPwd: string = "root";
        mysqlDbname: string = "tesdb001";
        wxServiceAddr: string = "ws://127.0.0.1:1443";
        allowedAppids: string[] = ["1.00002", "1.00003"];
        description: string = "医疗小程序";
        
    }
    export class AgentInfoResp extends MsgBase {
        result: number = 1;
        msg: string = 'ok';
    }
}


export = Message;
