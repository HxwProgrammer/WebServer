/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import express from 'express';
import MysqlHandler from './utils/mysqlHandler';
import logger from './logger';
const app = express();
// 设置静态页面的路径

var mysql = new MysqlHandler('ws://172.16.3.19:2020');
app.use('/*', (req, res, next) => {
    logger.info('url: ', req.originalUrl.toString());
    logger.info('baseUrl: ', req.baseUrl);
    logger.info('path: ', req.path);
    logger.info('params: ', req.params);
    logger.info('query: ', req.query);
    next();
});
app.use(express.static('static'));


app.get('/middle/test', (req, res, next) => {
    logger.info(`/middle/test 1`);
    next();
});

app.get('/middle/test', (req, res) => {
    logger.info(`/middle/test 2`);
    res.send('ok');
});


app.get('/WebTesting/Service/getServiceList', (req, res) => {
    res.set('Content-Type', 'text/json');
    mysql.getServiceList(res);
});

app.post('/WebTesting/Service/addServices', (req, res) => {
    mysql.addServices(req, res);
});
app.delete('/WebTesting/Service/delServices', (req, res) => {
    mysql.delServices(req, res)
});
app.post('/WebTesting/Service/updateService', (req, res) => {
    mysql.updateService(req, res);
});


app.get('/WebTesting/Service/getServices', (req, res, next) => {
    // 作内容检查.
    next();
});
app.get('/WebTesting/Service/getServices', (req, res) => {
    res.set('Content-Type', 'text/json');
    mysql.getServices((sl): void => {
        res.send(JSON.stringify(sl));
    });
});

class HttpServer {
    constructor() {
    }
    start(nPort: number) {
        app.listen(nPort, () => {
            logger.info('running...');
        });
    }
}

export = HttpServer;